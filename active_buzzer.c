/*********************ACTIVE BUZZER***********************************/

#include "platform.h"
#include "gpio.h"        // includes defination of gpio pins and read, write functions
#include "uart.h"


extern void DelayLoop(unsigned long cntr1, unsigned long cntr2);


void main()
{
    // Assume 0 ---> Input and
    // 1 ---> Output
    write_word(GPIO_DIRECTION_CNTRL_REG, 0x00000001);

    while(1)
    {
	write_word(GPIO_DATA_REG, 0x00);
	DelayLoop(1000, 1000);
        write_word(GPIO_DATA_REG, 0x00000001);
        DelayLoop(1000,1000);
        printf("\nWORKED SUCCESSFULLY");
    }
}
